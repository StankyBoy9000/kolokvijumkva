import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { KnjigaService } from './services/knjiga/knjiga.service';
import { IznajmljivanjeService } from './services/iznajmljivanje/iznajmljivanje.service';
import { ClanService } from './services/clan/clan.service';


@NgModule({
  declarations: [
    AppComponent,
    routingComponents
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [KnjigaService, IznajmljivanjeService, ClanService],
  bootstrap: [AppComponent]
})
export class AppModule { }
