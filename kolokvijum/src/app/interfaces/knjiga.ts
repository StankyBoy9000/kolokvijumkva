export interface Knjiga {
    id: number,
    naslov: string,
    autor: string,
    isbn: string
}
