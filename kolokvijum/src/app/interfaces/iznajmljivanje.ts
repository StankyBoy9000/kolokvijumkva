export interface Iznajmljivanje {
    id: number,
    knjigaId: number,
    clanId: number,
    datumVracanja: Date,
    datumIznajmljivanja: Date
}
