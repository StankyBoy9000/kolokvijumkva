import { Component, OnInit } from '@angular/core';
import { Knjiga } from 'src/app/interfaces/knjiga';
import { KnjigaService } from 'src/app/services/knjiga/knjiga.service';
import { ActivatedRoute } from '@angular/router';
import { IznajmljivanjeService } from 'src/app/services/iznajmljivanje/iznajmljivanje.service';
import { ClanService } from 'src/app/services/clan/clan.service';

@Component({
  selector: 'app-knjiga-details',
  templateUrl: './knjiga-details.component.html',
  styleUrls: ['./knjiga-details.component.css']
})
export class KnjigaDetailsComponent implements OnInit {
  public iznajmljivanja = [];
  public clanovi = [];
  public konkretnaIznajmljivanja = []
  public knjiga: Knjiga = {
    id:0,
    naslov:"",
    autor: "",
    isbn: ""
  };

  constructor(private _knjigaService: KnjigaService,private _iznajmljivanjeService: IznajmljivanjeService, private _clanService: ClanService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.getData();
    this.getIznajmljivanja();
    this.getClanovi();
  }


  getIznajmljivanja(){
    this._iznajmljivanjeService.getIznajmljivanja().subscribe( data => {this.iznajmljivanja = data
      this.filterIznajmljivanja()});
    
  }

  getClanovi(){
    this._clanService.getClanovi().subscribe( data => this.clanovi = data);
  }

  filterIznajmljivanja(){
    console.log(this.iznajmljivanja)
    for(let i = 0; i < this.iznajmljivanja.length; i++){
      if(this.iznajmljivanja[i].knjigaId =! this.knjiga.id || !this.iznajmljivanja[i].datumVracanja == null){
        this.iznajmljivanja.splice(i,1);
      }
    }
    console.log(this.iznajmljivanja)
  }
  getData(){
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this._knjigaService.getKnjiga(id).subscribe( data => this.knjiga = data);
  }

  editKnjiga(){
    this._knjigaService.updateKnjiga(this.knjiga.id,this.knjiga).subscribe(() => this.getData());
  }

  getClan(id){
    for(let i = 0; i < this.clanovi.length; i++){
      if(this.clanovi[i].id  == id){
        return this.clanovi[i].ime;
      }
    }
    return ""
  }
}
