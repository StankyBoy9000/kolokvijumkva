import { Component, OnInit } from '@angular/core';
import { Clan } from 'src/app/interfaces/clan';
import { ClanService } from 'src/app/services/clan/clan.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-clan-details',
  templateUrl: './clan-details.component.html',
  styleUrls: ['./clan-details.component.css']
})
export class ClanDetailsComponent implements OnInit {
  public clan: Clan = {
    id:0,
    ime:"",
    prezime: ""
  };

  constructor(private _clanService: ClanService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.getData();
  }

  getData(){
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this._clanService.getClan(id).subscribe( data => this.clan = data);
  }

  editClan(){
    this._clanService.updateClan(this.clan.id,this.clan).subscribe(() => this.getData());
  }

}
