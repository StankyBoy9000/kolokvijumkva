import { Component, OnInit } from '@angular/core';
import { Knjiga } from 'src/app/interfaces/knjiga';
import { KnjigaService } from 'src/app/services/knjiga/knjiga.service';
import { Router } from '@angular/router';
import { IznajmljivanjeService } from 'src/app/services/iznajmljivanje/iznajmljivanje.service';

@Component({
  selector: 'app-knjiga',
  templateUrl: './knjiga.component.html',
  styleUrls: ['./knjiga.component.css']
})
export class KnjigaComponent implements OnInit {

  public knjige = [];
 
  public izabranaKnjiga: Knjiga;
  public knjiga: Knjiga = {
    id:0,
    naslov:"",
    autor: "",
    isbn: ""
  };

  constructor( private _knjigaService: KnjigaService,  private router: Router) { }

  ngOnInit(): void {
    this.getKnjige();
  }

  

  getKnjige(){
    this._knjigaService.getKnjige().subscribe( data => this.knjige = data);
  }

  deleteKnjiga(id){
    this._knjigaService.deleteKnjiga(id).subscribe(() =>this.getKnjige());
  }

  addKnjiga(){
    this._knjigaService.addKnjiga(this.knjiga).subscribe(() => this.getKnjige());
  }

  knjigaDetails(knjiga){
    this.router.navigate(['/knjiga', knjiga.id]);
  }

}
