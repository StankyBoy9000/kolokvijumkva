import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IznajmljivanjeDetailsComponent } from './iznajmljivanje-details.component';

describe('IznajmljivanjeDetailsComponent', () => {
  let component: IznajmljivanjeDetailsComponent;
  let fixture: ComponentFixture<IznajmljivanjeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IznajmljivanjeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IznajmljivanjeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
