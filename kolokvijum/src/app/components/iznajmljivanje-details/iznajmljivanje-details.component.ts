import { Component, OnInit } from '@angular/core';
import { Iznajmljivanje } from 'src/app/interfaces/iznajmljivanje';
import { ActivatedRoute } from '@angular/router';
import { IznajmljivanjeService } from 'src/app/services/iznajmljivanje/iznajmljivanje.service';
import { ClanService } from 'src/app/services/clan/clan.service';
import { KnjigaService } from 'src/app/services/knjiga/knjiga.service';
import { Clan } from 'src/app/interfaces/clan';
import { Knjiga } from 'src/app/interfaces/knjiga';

@Component({
  selector: 'app-iznajmljivanje-details',
  templateUrl: './iznajmljivanje-details.component.html',
  styleUrls: ['./iznajmljivanje-details.component.css']
})
export class IznajmljivanjeDetailsComponent implements OnInit {

  public iznajmljivanje: Iznajmljivanje = {
    id:0,
    knjigaId: 0,
    clanId: 0,
    datumVracanja: null,
    datumIznajmljivanja: null
  };

  public clanovi = [];
  public knjige = [];

  public izabranClan: Clan = {
    id:0,
    ime:"",
    prezime: ""
  };
  public izabranaKnjiga: Knjiga = {
    id:0,
    naslov:"",
    autor: "",
    isbn: ""
  };

  constructor( private _iznajmljivanjeService: IznajmljivanjeService, private _clanService: ClanService, private _knjigaService: KnjigaService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    
    this.getData();
    
  }

  getData(){
    let id = parseInt(this.route.snapshot.paramMap.get('id'));
    this._iznajmljivanjeService.getIznajmljivanje(id).subscribe( data => this.iznajmljivanje = data);
    this.setIzabranClan();
    this.setIzabranaKnjiga();
  }

  editIznajmljivanje(){
    this._iznajmljivanjeService.updateIznajmljivanje(this.iznajmljivanje.id,this.iznajmljivanje).subscribe(() => this.getData());
  }

  getClanovi(){
    this._clanService.getClanovi().subscribe( data => this.clanovi = data);
  }

  getKnjige(){
    this._knjigaService.getKnjige().subscribe( data => this.knjige = data);
  }

  setIzabranClan(){

    this._clanService.getClan(this.iznajmljivanje.clanId).subscribe( data => this.izabranClan = data);
    
  }

  setIzabranaKnjiga(){
    this._knjigaService.getKnjiga(this.iznajmljivanje.knjigaId).subscribe( data => this.izabranaKnjiga = data);
  }
}
