import { Component, OnInit } from '@angular/core';
import { Iznajmljivanje } from 'src/app/interfaces/iznajmljivanje';
import { Router } from '@angular/router';
import { IznajmljivanjeService } from 'src/app/services/iznajmljivanje/iznajmljivanje.service';
import { ClanService } from 'src/app/services/clan/clan.service';
import { KnjigaService } from 'src/app/services/knjiga/knjiga.service';
import { Clan } from 'src/app/interfaces/clan';
import { Knjiga } from 'src/app/interfaces/knjiga';

@Component({
  selector: 'app-iznajmljivanje',
  templateUrl: './iznajmljivanje.component.html',
  styleUrls: ['./iznajmljivanje.component.css']
})
export class IznajmljivanjeComponent implements OnInit {
  public clanovi = [];
  public knjige = [];
  public iznajmljivanja = [];
  public izabranClan: Clan = {
    id:0,
    ime:"",
    prezime: ""
  };
  public izabranaKnjiga: Knjiga = {
    id:0,
    naslov:"",
    autor: "",
    isbn: ""
  };
  public iznajmljivanje: Iznajmljivanje = {
    id:0,
    knjigaId: 0,
    clanId: 0,
    datumVracanja: null,
    datumIznajmljivanja: null
  };

  public isbn;

  public response: string;

  

  constructor( private _iznajmljivanjeService: IznajmljivanjeService, private _clanService: ClanService, private _knjigaService: KnjigaService, private router: Router) { }

  ngOnInit(): void {
    this.getIznajmljivanja();
    this.getClanovi();
    this.getKnjige();
  }

  getIznajmljivanja(){
    this._iznajmljivanjeService.getIznajmljivanja().subscribe( data => this.iznajmljivanja = data);
  }

  deleteIznajmljivanje(id){
    this._iznajmljivanjeService.deleteIznajmljivanje(id).subscribe(() =>this.getIznajmljivanja());
  }

  addIznajmljivanje(){
    this.iznajmljivanje.knjigaId = this.izabranaKnjiga.id
    this.iznajmljivanje.clanId = this.izabranClan.id
    this._iznajmljivanjeService.addIznajmljivanje(this.iznajmljivanje).subscribe(() => this.getIznajmljivanja());
  }

  iznajmljivanjeDetails(iznajmljivanje){
    this.router.navigate(['/iznajmljivanje', iznajmljivanje.id]);
  }

  getClanovi(){
    this._clanService.getClanovi().subscribe( data => this.clanovi = data);
  }

  getKnjige(){
    this._knjigaService.getKnjige().subscribe( data => this.knjige = data);
  }


  getClan(id){
    for(let i = 0; i < this.clanovi.length; i++){
      if(this.clanovi[i].id  == id){
        return this.clanovi[i].ime;
      }
    }
    return ""
  }

  getKnjiga(id){
    for(let i = 0; i < this.knjige.length; i++){
      if(this.knjige[i].id  == id){
        return this.knjige[i].naslov;
      }
    }
    return ""
  }

  brzoIznajmljivanje(){
      let currentId = 0;
      for(let i = 0; i < this.knjige.length; i++){
        if(this.isbn = this.knjige[i].isbn){
          currentId = this.knjige[i].id
        }
      }
      for(let i = 0; i < this.iznajmljivanja.length; i++){
        if(this.iznajmljivanja[i].knjigaId == currentId){
          if(this.iznajmljivanja[i].datumVracanja == null){
            this.response = "Neuspesno iznajmljena knjiga"
          }
        }
      }
      let temp: Iznajmljivanje = {
        id:999,
        knjigaId: currentId,
        clanId: 1,
        datumVracanja: null,
        datumIznajmljivanja: new Date()
      }

      this.iznajmljivanje.clanId = temp.clanId
      this.iznajmljivanje.knjigaId = temp.knjigaId
      this.iznajmljivanje.id = 999
      this.iznajmljivanje.datumVracanja = null
      this.iznajmljivanje.datumIznajmljivanja = new Date();
      this.addIznajmljivanje();

      this.response = "uspesno iznajmljena knjiga"
      return this.response
  }
}
