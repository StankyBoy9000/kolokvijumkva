import { Component, OnInit } from '@angular/core';
import { Clan } from 'src/app/interfaces/clan';
import { ClanService } from 'src/app/services/clan/clan.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-clan',
  templateUrl: './clan.component.html',
  styleUrls: ['./clan.component.css']
})
export class ClanComponent implements OnInit {

  public clanovi = [];
  public izabranClan: Clan;
  public clan: Clan = {
    id:0,
    ime:"",
    prezime: ""
  };

  constructor( private _clanService: ClanService, private router: Router) { }

  ngOnInit(): void {
    this.getClanovi();
  }

  getClanovi(){
    this._clanService.getClanovi().subscribe( data => this.clanovi = data);
  }

  deleteClan(id){
    this._clanService.deleteClan(id).subscribe(() =>this.getClanovi());
  }

  addClan(){
    this._clanService.addClan(this.clan).subscribe(() => this.getClanovi());
  }

  checkIfClanExists(id){
    for(let i = 0; i < this.clanovi.length; i++){
      if(id == this.clanovi[i].id){
        return true;
      }
    }
    return false;
  }

  clanDetails(clan){
    this.router.navigate(['/clan', clan.id]);
  }

  selectClan(x){
    this.selectClan = x;
  }

}
