import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Iznajmljivanje } from 'src/app/interfaces/iznajmljivanje';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class IznajmljivanjeService {

  private _url: string = 'http://localhost:3000/iznajmljivanje'

  constructor(private http: HttpClient) { }

  getIznajmljivanja(): Observable<Iznajmljivanje[]>{
    return this.http.get<Iznajmljivanje[]>(this._url);
  }

  getIznajmljivanje(id): Observable<Iznajmljivanje>{
    return this.http.get<Iznajmljivanje>(this._url+"/"+id);
  }

  deleteIznajmljivanje(id): Observable<Iznajmljivanje>{
    return this.http.delete<Iznajmljivanje>(this._url+"/"+id);
  }

  addIznajmljivanje(iznajmljivanje: Iznajmljivanje): Observable<Iznajmljivanje>{
    return this.http.post<Iznajmljivanje>(this._url, iznajmljivanje);
  }

  updateIznajmljivanje(id, iznajmljivanje): Observable<Iznajmljivanje>{
    return this.http.put<Iznajmljivanje>(`http://localhost:3000/iznajmljivanje/${id}`, iznajmljivanje);
  }
}
