import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Knjiga } from 'src/app/interfaces/knjiga';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class KnjigaService {

  private _url: string = 'http://localhost:3000/knjiga'
  constructor(private http: HttpClient) { }

  getKnjige(): Observable<Knjiga[]>{
    return this.http.get<Knjiga[]>(this._url);
  }

  getKnjiga(id): Observable<Knjiga>{
    return this.http.get<Knjiga>(this._url+"/"+id);
  }

  deleteKnjiga(id): Observable<Knjiga>{
    return this.http.delete<Knjiga>(this._url+"/"+id);
  }

  addKnjiga(knjiga: Knjiga): Observable<Knjiga>{
    return this.http.post<Knjiga>(this._url, knjiga);
  }

  updateKnjiga(id, knjiga): Observable<Knjiga>{
    return this.http.put<Knjiga>(`http://localhost:3000/knjiga/${id}`,knjiga);
  }
}
