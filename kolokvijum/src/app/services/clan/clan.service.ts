import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { Clan } from 'src/app/interfaces/clan';

@Injectable({
  providedIn: 'root'
})
export class ClanService {

  private _url: string = 'http://localhost:3000/clan'

  constructor(private http: HttpClient) { }

  getClanovi(): Observable<Clan[]>{
    return this.http.get<Clan[]>(this._url);
  }

  getClan(id): Observable<Clan>{
    return this.http.get<Clan>(this._url+"/"+id);
  }

  deleteClan(id): Observable<Clan>{
    return this.http.delete<Clan>(this._url+"/"+id);
  }

  addClan(clan: Clan): Observable<Clan>{
    return this.http.post<Clan>(this._url, clan);
  }

  updateClan(id, clan): Observable<Clan>{
    return this.http.put<Clan>(`http://localhost:3000/clan/${id}`, clan);
  }
}
