import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KnjigaComponent } from './components/knjiga/knjiga.component';
import { KnjigaDetailsComponent } from './components/knjiga-details/knjiga-details.component';
import { ClanComponent } from './components/clan/clan.component';
import { ClanDetailsComponent } from './components/clan-details/clan-details.component';
import { IznajmljivanjeComponent } from './components/iznajmljivanje/iznajmljivanje.component';
import { IznajmljivanjeDetailsComponent } from './components/iznajmljivanje-details/iznajmljivanje-details.component';


const routes: Routes = [
  {path: 'knjiga', component: KnjigaComponent},
  {path: 'knjiga/:id', component: KnjigaDetailsComponent},
  {path: 'clan', component: ClanComponent},
  {path: 'clan/:id', component: ClanDetailsComponent},
  {path: 'iznajmljivanje', component: IznajmljivanjeComponent},
  {path: 'iznajmljivanje/:id', component: IznajmljivanjeDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [
  KnjigaComponent,
  KnjigaDetailsComponent,
  ClanComponent,
  ClanDetailsComponent,
  IznajmljivanjeComponent,
  IznajmljivanjeDetailsComponent
]
